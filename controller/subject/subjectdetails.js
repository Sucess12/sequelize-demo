const Sequelize = require("sequelize");
const db = require("../../models");
const subjectD = db.subject;

// var db = {};
// db.Sequelize=Sequelize;
// db.seq=seq;

const subjectObj = {
  createSubject: async (req, res) => {
    try {
      console.log("in create");
     var createObj = req.body;
      let item = await subjectD.create(createObj);
      console.log("item...................",item);
      res.send("success");
    } 
    catch (error) {
      res.send(error);
    }
  },
  getAll: async (req, res) => {
    try {
    
      let item = await subjectD.findAll();
      res.send(item);
    } 
    catch (error) {
      res.send(error);
    }
  },
  update: async (req, res) => {
    try {
        var itemId=req.params.id;
     var updateObj = req.body;
      let item = await subjectD.update(updateObj,{where:{id:itemId}});
      res.send("success");
    } 
    catch (error) {
      res.send(error);
    }
  },
  delete: async (req, res) => {
    try {
        id=req.params.id;
  
        let query = {
            where: {
              id: id,
            },
          };
          let data = await subjectD.destroy(query);
          res.send("deleted successfully");
   
    } 
    catch (error) {
      res.send(error);
    }
  },
};

module.exports = subjectObj;
