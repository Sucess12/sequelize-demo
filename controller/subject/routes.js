const app = require("express").Router();
//const router= app.Router();
const subject =require('./subjectdetails');

app.post(
    '/createSubject',
    subject.createSubject

);
app.get(
    '/getAll',
    subject.getAll

);
app.put(
    '/updateSubject/:id',
    subject.update

);
app.delete(
    '/deleteSubject/:id',
    subject.delete

);

module.exports=app;