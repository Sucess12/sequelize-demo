const Sequelize = require("sequelize");
const db = require("../../models");
const departmentD = db.department;

const departmentObj = {
  createdepartment: async (req, res) => {
    try {
      console.log("in create");
     var createObj = req.body;
      let item = await departmentD.create(createObj);
      console.log("item...................",item);
      res.send("success");
    } 
    catch (error) {
      res.send(error);
    }
  },
  getAll: async (req, res) => {
    try {
    
      let item = await departmentD.findAll();
      res.send(item);
    } 
    catch (error) {
      res.send(error);
    }
  },
  update: async (req, res) => {
    try {
        var itemId=req.params.D_id;
     var updateObj = req.body;
      let item = await departmentD.update(updateObj,{where:{D_id:itemId}});
      res.send("success");
    } 
    catch (error) {
      res.send(error);
    }
  },
  delete: async (req, res) => {
    try {
        itemid=req.params.D_id;
  
        let query = {
            where: {
              D_id: itemid,
            },
          };
          let data = await departmentD.destroy(query);
          res.send("deleted successfully");
   
    } 
    catch (error) {
      res.send(error);
    }
  },
};

module.exports = departmentObj;
