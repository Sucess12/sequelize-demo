const app = require("express").Router();
//const router= app.Router();
const department =require('./departmentdetails');

app.post(
    '/createdepartment',
    department.createdepartment

);
app.get(
    '/getAll',
    department.getAll

);
app.put(
    '/updatedepartment/:D_id',
    department.update

);
app.delete(
    '/deletedepartment/:D_id',
    department.delete

);

module.exports=app;