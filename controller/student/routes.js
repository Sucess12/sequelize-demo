const app = require("express").Router();
//const router= app.Router();
const student =require('./studentdetails');

app.post(
    '/createstudent',
    student.createstudent

);
app.get(
    '/getAll',
    student.getAll

);
app.put(
    '/updateStudent/:id',
    student.update

);
app.delete(
    '/deleteStudent/:id',
    student.delete

);

module.exports=app;