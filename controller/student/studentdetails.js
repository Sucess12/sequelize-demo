const Sequelize = require("sequelize");
const db = require("../../models");
const studentD = db.student;

// var db = {};
// db.Sequelize=Sequelize;
// db.seq=seq;

const studentObj = {
  createstudent: async (req, res) => {
    try {
      console.log("in create");
     var createObj = req.body;
      let item = await studentD.create(createObj);
      console.log("item...................",item);
      res.send("success");
    } 
    catch (error) {
      res.send(error);
    }
  },
  getAll: async (req, res) => {
    try {
    
      let item = await studentD.findAll();
      res.send(item);
    } 
    catch (error) {
      res.send(error);
    }
  },
  update: async (req, res) => {
    try {
        var itemId=req.params.id;
     var updateObj = req.body;
      let item = await studentD.update(updateObj,{where:{id:itemId}});
      res.send("success");
    } 
    catch (error) {
      res.send(error);
    }
  },
  delete: async (req, res) => {
    try {
        id=req.params.id;
  
        let query = {
            where: {
              id: id,
            },
          };
          let data = await studentD.destroy(query);
          res.send("deleted successfully");
   
    } 
    catch (error) {
      res.send(error);
    }
  },
};

module.exports = studentObj;
