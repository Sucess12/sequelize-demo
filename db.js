var mysql=require('mysql');
var connection = mysql.createPool({
    host:'localhost',
    port:'3306',
    user:'root',
    database:'studentdb',
    password:'root'
});

connection.getConnection(function(error,connection){
    if(error)
    return error;
    return connection;
});

module.exports= connection;