module.exports = (seq,DataTypes)=>{

    console.log("here fees");
const fees = seq.define('fees', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    fees: {
        type: DataTypes.STRING,
        allowNull: false
    },
    paid: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    balance: {
        type: DataTypes.STRING,
        allowNull: false
    },
},
{
    timestamps:false
});


return fees;
}
