var {Sequelize,DataTypes}=require('sequelize')
const fs = require('fs');
const path = require('path');

const db={};
const seq=new Sequelize('studentdb','root','root',{
    host:'localhost',
    dialect:'mysql'
});

seq.authenticate()
.then(()=>{
    console.log('Database connected....');
})
.catch(err=>{
    console.log('Error:'+err);
});

const files = fs.readdirSync(__dirname);


files.forEach((filename) => {
  if (filename != path.basename(__filename)) {
    let mypath = path.join(__dirname, filename);
    let model = require(mypath)(seq, Sequelize.DataTypes);
    db[model.name] = model;
    model.sync();
  }
});



Object.keys(db)
    .forEach((modelName) => {
        if (db[modelName].associate) {
            db[modelName].associate(db);
        }
    });



db.Sequelize=Sequelize;
db.seq=seq;
// db.student=require('../models/student')(seq,DataTypes);



module.exports=db;