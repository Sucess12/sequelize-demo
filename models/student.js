module.exports = (seq,DataTypes)=>{
    console.log("here student");
const student = seq.define('student', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    rollno: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
},
{
    timestamps:false
});

seq.sync({force:false, alter:true, logging:true});
return student;
}
