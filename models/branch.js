module.exports = (seq,DataTypes)=>{

    console.log("here branch ");
const branch= seq.define('branch', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    electronics: {
        type: DataTypes.STRING,
        allowNull: false
    },
    mechanical: {
        type: DataTypes.STRING,
        allowNull: false
    },
    electrical: {
        type: DataTypes.STRING,
        allowNull: false
    },
    computer: {
        type: DataTypes.STRING,
        allowNull: false
    },
},
{
    timestamps:false
   
});


return branch;
}
