module.exports = (seq,DataTypes)=>{
    console.log("here college");
const college = seq.define('college', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    rollno: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
},
{
    timestamps:false
});


return college;
}
