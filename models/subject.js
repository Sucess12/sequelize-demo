module.exports = (seq,DataTypes)=>{

    console.log("here  subjects");
    const subject = seq.define('subject', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    english: {
        type: DataTypes.STRING,
        allowNull: false
    },
    maths: {
        type: DataTypes.STRING,
        allowNull: false
    },
    physics: {
        type: DataTypes.STRING,
        allowNull: false
    },
},
{
    timestamps:false,
    tableName:'subject'
});


return subject;
}
